describe('Perfom different API call types', () =>{

  it('GET - read', () => {
    cy.request('http://dummy.restapiexample.com/api/v1/employees').then((response) => {
      expect(response).to.have.property('status', 200)
      //expect(response.body).to.not.be.null
      //expect(response.body.data).to.have.length(24)
    })
  })

  it('POST - create', () => {
    const item = {"name":"simphiwe","salary":"323456","age":"41"}
    cy.request('POST', 'http://dummy.restapiexample.com/api/v1/create', item)
    .its('body')
    .its('data')
    //.should('deep.equal',item)
    .should('include',{name:'simphiwe'})
  })

  it('PUT - udate', ()=> {
    const item2 = {"name": "thando"}
    cy.request('PUT', 'http://dummy.restapiexample.com/api/v1/update/1', item2).its('status').should('eq', 401)
  })
})
